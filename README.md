# 实证分析可视化

#### 介绍
连玉君，实证分析可视化，课件，codes

- 实证分析可视化
  - 为什要可视化？
  - Stata 绘图命令的架构
  - 直方图与密度函数图：histogram, kdensity, biplot
  - 分仓散点图：binscatter，binscatter2
  - 系数及系数差异的可视化呈现：coefplot
  - 调节效应、倒 U 型关系及边际效应的可视化
  - 面板数据、多个控制变量、高维固定效应模型的可视化

> &#x1F34E;  [连玉君-实证分析可视化-slides.pdf](https://gitee.com/arlionn/graph/blob/master/%E8%BF%9E%E7%8E%89%E5%90%9B_Slides_%E5%AE%9E%E8%AF%81%E5%88%86%E6%9E%90%E5%8F%AF%E8%A7%86%E5%8C%96.pdf)    
> &#x1F34F;  [Lian_Stata_graph.do](https://gitee.com/arlionn/graph/blob/master/Lian_Stata_graph.do)

&emsp;

- 专题：[Stata教程](https://www.lianxh.cn/blogs/17.html)
  - [Stata绘图：环形柱状图-实时全球新冠确诊人数](https://www.lianxh.cn/news/b33c479a0a6de.html)
  - [普林斯顿Stata教程(二) - Stata绘图](https://www.lianxh.cn/news/64495e9c4801c.html)
- 专题：[Stata绘图](https://www.lianxh.cn/blogs/24.html)
  - [Stata绘图：绘制二维地图](https://www.lianxh.cn/news/523be8e005bca.html)
  - [Stata绘图：峰峦图绘制 joy_plot](https://www.lianxh.cn/news/ae974713dbd40.html)
  - [Stata绘图：绘制华夫饼图-waffle](https://www.lianxh.cn/news/beb993d5b649e.html)
  - [Stata绘图：绘制美观的散点图-superscatter](https://www.lianxh.cn/news/b8ca39e205d86.html)
  - [Stata绘图：回归系数可视化-multicoefplot](https://www.lianxh.cn/news/3e052bd3291dd.html)
  - [Stata绘图：如何更高效的绘制图形](https://www.lianxh.cn/news/07bfc4c9db38b.html)
  - [Stata绘图：绘制桑基图-sankey_plot](https://www.lianxh.cn/news/5e628ee91f098.html)
  - [Stata绘图-可视化：组间差异比较散点图](https://www.lianxh.cn/news/b5fc0aeb1d7b7.html)
  - [Stata绘图：addplot-fabplot-多图层美化图片](https://www.lianxh.cn/news/0309ba204eb8e.html)
  - [Stata绘图：箱形图与小提琴图-vioplot](https://www.lianxh.cn/news/fe238b4d418c5.html)
  - [Stata绘图：太美了！羊皮卷风格图形](https://www.lianxh.cn/news/7d19377d49d52.html)
  - [Stata绘图：自定义绘图利器-palettes](https://www.lianxh.cn/news/4a9a119c6fbd8.html)
  - [史上最牛Stata绘图模版-schemepack：酷似R中的ggplot2-袁子晴](https://www.lianxh.cn/news/e76a8a7e3c6c4.html)
  - [Stata绘图：addplot-层层叠加轻松绘图](https://www.lianxh.cn/news/342e156beda06.html)
  - [Stata 绘图：用 Stata 绘制一打精美图片-schemes](https://www.lianxh.cn/news/0f2537275983f.html)
  - [常用科研统计绘图工具介绍](https://www.lianxh.cn/news/021856c11e46b.html)
  - [Stata空间计量：莫兰指数绘图moranplot命令介绍](https://www.lianxh.cn/news/b17e298a7b346.html)
  - [Stata绘图-组间差异可视化：不良事件火山图、点阵图](https://www.lianxh.cn/news/f9680c4be14fe.html)
  - [Stata绘图极简新模板：plotplain和plottig-T251](https://www.lianxh.cn/news/37ea99dab6efb.html)
  - [给你的图形化个妆：Stata绘图常用选项汇总-上篇](https://www.lianxh.cn/news/59bc1ff7d027a.html)
  - [给你的图形化个妆：Stata绘图常用选项汇总-下篇](https://www.lianxh.cn/news/180daa074ef27.html)
  - [Stata绘图：柱状图专题-T212](https://www.lianxh.cn/news/df9729c5de34d.html)
  - [Stata绘图：回归系数可视化-论文更出彩](https://www.lianxh.cn/news/324f5e3053883.html)
  - [Stata绘图：世行可视化案例-条形图-密度函数图-地图-断点回归图-散点图](https://www.lianxh.cn/news/96989b0de4d83.html)
  - [Stata绘图：随机推断中的系数可视化](https://www.lianxh.cn/news/ce93c41862c16.html)
  - [Stata绘图：重新定义坐标轴刻度标签](https://www.lianxh.cn/news/c32c903235e7d.html)
  - [Stata绘图：用-bytwoway-实现快速分组绘图](https://www.lianxh.cn/news/4d72752d51859.html)
  - [Stata绘图：一个干净整洁的-Stata-图形模板qlean](https://www.lianxh.cn/news/9300f2b9f1960.html)
  - [Stata绘图：怎么在Stata图形中附加水平线或竖直线？](https://www.lianxh.cn/news/34bb158641831.html)
  - [Stata绘图：在图片中添加虚线网格线](https://www.lianxh.cn/news/126f5d3d6fae5.html)
  - [Stata绘图：制作教学演示动态图-GIF](https://www.lianxh.cn/news/a0cd3eaaecf80.html)
  - [Stata绘图：绘制一颗红心-姑娘的生日礼物](https://www.lianxh.cn/news/e6a97fe962611.html)
  - [Stata绘图：bgshade命令-在图形中加入经济周期阴影](https://www.lianxh.cn/news/b89fd3c601e31.html)
  - [Stata绘图：让图片透明——你不要掩盖我的光芒](https://www.lianxh.cn/news/ed0d5216d7eb7.html)
  - [Stata：图形美颜-自定义绘图模板-grstyle-palettes](https://www.lianxh.cn/news/8c1819ff61a8a.html)
  - [Stata绘图：多维柱状图绘制](https://www.lianxh.cn/news/01fab25337d76.html)
  - [Stata绘图：用暂元统一改变图形中的字号](https://www.lianxh.cn/news/dabe656d89faf.html)
  - [一文看尽 Stata 绘图](https://www.lianxh.cn/news/ae36a721cfe18.html)
  - [Stata绘图：绘制单个变量的时序图](https://www.lianxh.cn/news/f62630d968b9e.html)

