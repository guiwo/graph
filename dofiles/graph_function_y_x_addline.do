
*-Stata绘图：唯美的函数图-自定义水平和竖直附加线.md 

* 连玉君 2022/9/21 15:22
* https://www.lianxh.cn

* 在 Stata 命令窗口中输入如下命令可以查看对应推文：

  lianxh 唯美的函数图 


*----------------Let's go -------------
 global path "D:\Lec\0-可视化\function-y-x"
 cap !md "$path/out"
 cd "$path/out"


* 安装和设置绘图模板 (可以忽略)
/*
. set scheme s2color  // Stata 默认, 彩色
. set scheme s2mono   // Stata 默认, 黑白
  net install scheme_scientific.pkg, replace  //安装绘图模板
. set scheme  scientific //本文使用的模板
*/   
*--------------------------------------


global width = 2000  // 图片宽度, LaTeX
global width =  700  // 图片宽度, blog
global dash "lp(dash) lc(gray) lw(*0.8)" //虚线风格

*-## 1. 简介
#d ;
*global dash "lp(dash) lc(gray) lw(*0.8)";
twoway 
  (function y=2*x , range(0 4) lp(solid))
  (function y=2, range(0 1) $dash) 
  (function y=1, range(0 2) $dash horizontal) 
  (function y=6, range(0 3) $dash) 
  (function y=3, range(0 6) $dash horizontal)   
  ,
  ylabel(, nogrid)
  aspect(0.8) 
  legend(off);
#d cr 
graph export "function_addline_01.png", ///
      width($width) replace  

      
*-## 2. yline() 和 xline() 选项无法奏效
twoway                                  ///
  function y=2*x , range(0 4) lp(solid) ///
           yline(2, lp(dash) lc(blue))  ///
           yline(6, lp(dash) noextend)  ///
           xline(1, lp(dash) lc(blue))  ///
           xline(3, lp(dash) noextend)  ///
           aspect(0.8) 
graph export "function_addline_02.png", ///
      width($width) replace         

      
*-## 3. 使用 twoway function 命令      
*-水平辅助线
twoway function y=3, range(0.5 3)            ///
        xlabel(0(1)4) ylabel(0(1)4) aspect(0.8)
graph export "function_addline_03a.png", width($width) replace 

*-竖直辅助线
twoway function y=3, range(0.5 3) horizontal ///
       xlabel(0(1)4) ylabel(0(1)4) aspect(0.8)
graph export "function_addline_03b.png", width($width) replace


*-## 4. 例子：正态分布的密度函数图和分布图

*--------------------------------------
* set scheme scientific 
#d ;
twoway
  (function y = 1.2 +normal(x), range(-4 +4)   lp(solid) lc(blue))
  (function y = 2*normalden(x), range(-4 +4)   lp(solid))            
  (function y = 2*normalden(x), range(-4 -0.6) lp(solid)  
                                recast(area) color(balck*0.6%30))
  (function y = 1.2 +normal(-0.6), range(-4 -0.6) lp(dash) lc(gray) lw(*0.6))
  (function y =-0.6, range(0 1.4742) horizontal lp(dash)   lc(gray) lw(*0.6))
  (pcarrowi 0.3 -2.2 0.15 -1.3, lc(black%50) mcolor(black%50))
  ,
  xlabel(-4 "-{&infin}" 0 +4 "+{&infin}")
  xtitle("",placement(3))
  ylabel(1.185 "{bf:0}" 1.4742 "{it:F}(x1)" 2.2 "{bf:1}", nogrid)
  yline( 1.185, lp(solid) lc(black) lw(*0.4))
  plotregion(margin(b-1.0)) //plotregion(margin(0))  //贴住底边
  aspect(1)  legend(off)
  text(2.0 1.0 "{it:F}(x)", place(e))
  text(0.7 0.8 "{it:f }(x)", place(e))
  text(1.14 -0.53 "x1", place(e))
  text(0.04 -0.53 "x1", place(e))
  text(1.14 4 "x", place(e)) 
  text(0.4 -1.65 "面积 = {it:F}(x1)", place(w)) ;
#d cr 
*--------------------------------------
graph export "function_addline_04.png", width($width) replace 


*-## 5. 添加带箭头的辅助线

*- U[0,1] --> N(0,1)
*--------------------------------------
* set scheme scientific 
#d ;
global arr "lp(dash) lw(*0.5) lc(gray) mcolor(gray)"; //箭头属性
local x1 =-1.4; local F_x1=normal(`x1')+1.2; 
local x2 = 0.0; local F_x2=normal(`x2')+1.2; 
local x3 = 1.3; local F_x3=normal(`x3')+1.2; 
twoway
  (function y = 1.2 +normal(x), range(-4 +4)   lp(solid) lc(black))
  (function y = 2.0*normalden(x), range(-4 +4) lp(solid))            
  (pcarrowi `F_x1' -4.0 `F_x1' `x1', $arr)
  (pcarrowi `F_x1' `x1'     0  `x1', $arr)
  (pcarrowi `F_x2' -4.0 `F_x2' `x2', $arr)
  (pcarrowi `F_x2' `x2'     0  `x2', $arr) 
  (pcarrowi `F_x3' -4.0 `F_x3' `x3', $arr)
  (pcarrowi `F_x3' `x3' 0 `x3', $arr)   
  ,
  xtitle("", placement(3) margin(t-4))
  xlabel(-4 "-{&infin}" `x1' 0 `x3' 4 "+{&infin}")
  ylabel(1.185 "{bf:0}" 1.28 "0.08" 1.7 "0.50" 
         2.103 "0.90" 2.2 "{bf:1}",nogrid)
  yline( 1.185, lp(solid) lc(black) lw(*0.4))
  plotregion(margin(l-1.68 b-1.28))
  aspect(0.8)  
  legend(off)
  text(2.1  3.5 "{it:F}(x)" , place(e))
  text(0.1  3.5 "{it:f }(x)", place(e))  
  text(1.14 4 "x", place(e)) ;
#d cr 
*--------------------------------------
graph export "function_addline_05.png", width($width) replace 


*-## 6. 扩展应用：用 twoway function 绘制正方形

*-Variance v.s Bias 方差-偏差组合

clear
set seed 135
set obs 15
input x0  y0
      1   1
      3.2 1
      1   3.2
      3.2 3.2
end 

gen x1 = runiform(0.5, 0.7)
gen y1 = runiform(2.8, 3.0)

gen x2 = runiform(3.2, 4.0)
gen y2 = runiform(3.2, 4.0)

gen x3 = runiform(2.8, 3.6)
gen y3 = runiform(0.6, 1.4)

gen x4 = runiform(0.9, 1.1)
gen y4 = runiform(0.9, 1.1)

#d ;
global dash "lp(solid) lc(gray) lw(*0.6)";
global dot  "mc(black%80) msize(*1)";
twoway 
  (scatter y1 x1, ms(oh) $dot )
  (scatter y2 x2, ms(oh) $dot )
  (scatter y3 x3, ms(oh) $dot )
  (scatter y4 x4, ms(oh) $dot )
  (scatter y0 x0, ms(D) mc(red%80) msize(*0.8))
  
  (function y=0, range(0 2) $dash) 
  (function y=0, range(0 2) $dash horizontal)
  (function y=2, range(0 2) $dash) 
  (function y=2, range(0 2) $dash horizontal) 
  
  (function y=2.2, range(0 2) $dash) 
  (function y=0,   range(2.2 4.2) $dash horizontal)
  (function y=4.2, range(0 2) $dash) 
  (function y=2,   range(2.2 4.2) $dash horizontal)  

  (function y=0,   range(2.2 4.2) $dash) 
  (function y=2.2, range(0 2) $dash horizontal)
  (function y=2,   range(2.2 4.2) $dash) 
  (function y=4.2, range(0 2) $dash horizontal) 
  
  (function y=2.2, range(2.2 4.2) $dash) 
  (function y=2.2, range(2.2 4.2) $dash horizontal)
  (function y=4.2, range(2.2 4.2) $dash) 
  (function y=4.2, range(2.2 4.2) $dash horizontal)  
  ,
  xlabel(1 "Low Variance" 3.2 "High Variance")  
  ylabel(1 "Low Bias" 3.2 "High Bias", nogrid angle(90))
  yscale(noline ) xscale(noline) 
  aspect(1) 
  legend(off);
#d cr 
graph export "function_addline_06.png", width($width) replace

