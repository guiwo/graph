
## 简介

这里存放了部分推文的 dofiles。

你可以点击本仓库点击右上角的【**Fork**】按钮，可以把这个项目完整复制到你的码云账号下，随时查看。 

相关推文：在 Stata 命令窗口中输入 `lianxh 绘图 可视化` 命令即可查看。 


&emsp;

## 相关推文

> Note：产生如下推文列表的 Stata 命令为：   
> &emsp; `lianxh 绘图 可视化`  
> 安装最新版 `lianxh` 命令：    
> &emsp; `ssc install lianxh, replace` 

- 专题：[Stata教程](https://www.lianxh.cn/blogs/17.html)
  - [普林斯顿Stata教程(二) - Stata绘图](https://www.lianxh.cn/news/64495e9c4801c.html)
- 专题：[数据处理](https://www.lianxh.cn/blogs/25.html)
  - [Stata：边际处理效应及其可视化-mtefe-T309](https://www.lianxh.cn/news/edd2a0cf9fa6a.html)
  - [Stata: 约翰霍普金斯大学 COVID-19 疫情数据处理及可视化](https://www.lianxh.cn/news/ca5082adbf97f.html)
- 专题：[Stata绘图](https://www.lianxh.cn/blogs/24.html)
  - [Stata绘图：复现组间均值差异图](https://www.lianxh.cn/news/5242699e2c424.html)
  - [Stata绘图：sunflower-向日葵图-克服散点重叠](https://www.lianxh.cn/news/04b0304d5a3be.html)
  - [Stata绘图：环形柱状图-实时全球新冠确诊人数](https://www.lianxh.cn/news/b33c479a0a6de.html)
  - [Stata绘图：绘制二维地图](https://www.lianxh.cn/news/523be8e005bca.html)
  - [Stata可视化：能用图形就不用表格](https://www.lianxh.cn/news/0edc0cc509220.html)
  - [Stata绘图：峰峦图绘制 joy_plot](https://www.lianxh.cn/news/ae974713dbd40.html)
  - [Stata绘图：绘制华夫饼图-waffle](https://www.lianxh.cn/news/beb993d5b649e.html)
  - [Stata绘图：绘制美观的散点图-superscatter](https://www.lianxh.cn/news/b8ca39e205d86.html)
  - [Stata绘图：回归系数可视化-multicoefplot](https://www.lianxh.cn/news/3e052bd3291dd.html)
  - [Stata绘图：如何更高效的绘制图形](https://www.lianxh.cn/news/07bfc4c9db38b.html)
  - [Stata绘图：绘制桑基图-sankey_plot](https://www.lianxh.cn/news/5e628ee91f098.html)
  - [Stata绘图-可视化：组间差异比较散点图](https://www.lianxh.cn/news/b5fc0aeb1d7b7.html)
  - [Stata绘图：addplot-fabplot-多图层美化图片](https://www.lianxh.cn/news/0309ba204eb8e.html)
  - [Stata绘图：箱形图与小提琴图-vioplot](https://www.lianxh.cn/news/fe238b4d418c5.html)
  - [Stata绘图：太美了！羊皮卷风格图形](https://www.lianxh.cn/news/7d19377d49d52.html)
  - [Stata绘图：自定义绘图利器-palettes](https://www.lianxh.cn/news/4a9a119c6fbd8.html)
  - [史上最牛Stata绘图模版-schemepack：酷似R中的ggplot2-袁子晴](https://www.lianxh.cn/news/e76a8a7e3c6c4.html)
  - [Stata可视化：biplot一图看尽方差、相关性和主成分](https://www.lianxh.cn/news/3173ebd034f12.html)
  - [Stata绘图：addplot-层层叠加轻松绘图](https://www.lianxh.cn/news/342e156beda06.html)
  - [Stata 绘图：用 Stata 绘制一打精美图片-schemes](https://www.lianxh.cn/news/0f2537275983f.html)
  - [常用科研统计绘图工具介绍](https://www.lianxh.cn/news/021856c11e46b.html)
  - [Stata空间计量：莫兰指数绘图moranplot命令介绍](https://www.lianxh.cn/news/b17e298a7b346.html)
  - [Stata绘图-组间差异可视化：不良事件火山图、点阵图](https://www.lianxh.cn/news/f9680c4be14fe.html)
  - [Stata绘图极简新模板：plotplain和plottig-T251](https://www.lianxh.cn/news/37ea99dab6efb.html)
  - [forest-森林图：分组回归系数可视化](https://www.lianxh.cn/news/472768848cd8d.html)
  - [给你的图形化个妆：Stata绘图常用选项汇总-上篇](https://www.lianxh.cn/news/59bc1ff7d027a.html)
  - [给你的图形化个妆：Stata绘图常用选项汇总-下篇](https://www.lianxh.cn/news/180daa074ef27.html)
  - [Stata绘图：柱状图专题-T212](https://www.lianxh.cn/news/df9729c5de34d.html)
  - [Stata绘图：回归系数可视化-论文更出彩](https://www.lianxh.cn/news/324f5e3053883.html)
  - [Stata绘图：世行可视化案例-条形图-密度函数图-地图-断点回归图-散点图](https://www.lianxh.cn/news/96989b0de4d83.html)
  - [Stata绘图：随机推断中的系数可视化](https://www.lianxh.cn/news/ce93c41862c16.html)
  - [Stata绘图：重新定义坐标轴刻度标签](https://www.lianxh.cn/news/c32c903235e7d.html)
  - [Stata绘图：用-bytwoway-实现快速分组绘图](https://www.lianxh.cn/news/4d72752d51859.html)
  - [Stata绘图：一个干净整洁的-Stata-图形模板qlean](https://www.lianxh.cn/news/9300f2b9f1960.html)
  - [Stata绘图：怎么在Stata图形中附加水平线或竖直线？](https://www.lianxh.cn/news/34bb158641831.html)
  - [Stata绘图：在图片中添加虚线网格线](https://www.lianxh.cn/news/126f5d3d6fae5.html)
  - [Stata绘图：制作教学演示动态图-GIF](https://www.lianxh.cn/news/a0cd3eaaecf80.html)
  - [Stata绘图：绘制一颗红心-姑娘的生日礼物](https://www.lianxh.cn/news/e6a97fe962611.html)
  - [Stata绘图：bgshade命令-在图形中加入经济周期阴影](https://www.lianxh.cn/news/b89fd3c601e31.html)
  - [Stata绘图：让图片透明——你不要掩盖我的光芒](https://www.lianxh.cn/news/ed0d5216d7eb7.html)
  - [Stata：图形美颜-自定义绘图模板-grstyle-palettes](https://www.lianxh.cn/news/8c1819ff61a8a.html)
  - [Stata绘图：多维柱状图绘制](https://www.lianxh.cn/news/01fab25337d76.html)
  - [Stata绘图：用暂元统一改变图形中的字号](https://www.lianxh.cn/news/dabe656d89faf.html)
  - [一文看尽 Stata 绘图](https://www.lianxh.cn/news/ae36a721cfe18.html)
  - [Stata绘图：绘制单个变量的时序图](https://www.lianxh.cn/news/f62630d968b9e.html)
- 专题：[结果输出](https://www.lianxh.cn/blogs/22.html)
  - [Stata可视化：让他看懂我的结果！](https://www.lianxh.cn/news/01607de7be5e8.html)
- 专题：[回归分析](https://www.lianxh.cn/blogs/32.html)
  - [Stata：在线可视化模拟-OLS-的性质](https://www.lianxh.cn/news/555995cc140a8.html)
- 专题：[面板数据](https://www.lianxh.cn/blogs/20.html)
  - [Stata绘图：面板数据可视化-panelview](https://www.lianxh.cn/news/78c21ab215c46.html)
- 专题：[Python-R-Matlab](https://www.lianxh.cn/blogs/37.html)
  - [Python 调用 API 爬取百度 POI 数据小贴士——坐标转换、数据清洗与 ArcGIS 可视化](https://www.lianxh.cn/news/a72842993b22b.html)
- 专题：[工具软件](https://www.lianxh.cn/blogs/23.html)
  - [知乎热议：有哪些一用就爱上的可视化工具？](https://www.lianxh.cn/news/67e8b9c15a0e9.html)
- 专题：[其它](https://www.lianxh.cn/blogs/33.html)
  - [数据可视化：带孩子们边玩边学吧](https://www.lianxh.cn/news/5a0f12c4ea08a.html)
- 专题：[公开课](https://www.lianxh.cn/blogs/53.html)
  - [⏩连享会公开课：实证研究中的数据可视化](https://www.lianxh.cn/news/9c86f40c6f17a.html)


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/底部图片_还上远帆001.png)
